export CC=/usr/bin/gcc-4.2
export EDITOR="vim"
export path="$HOME/bin:$PATH"
export PATH="$HOME/.rbenv/bin:$PATH"
export PERL_CPANM_OPT="--local-lib=~/perl5"
export PERL5LIB="/Users/hitoshi/perl5/lib/perl5:$PERL5LIB"
export GISTY_DIR="$HOME/Git.repos/gists"
export GISTY_ACCESS_TOKEN=2ed5764def2222c4030520436a08fb936d2ca497
export PYTHONPATH=/usr/local/lib/python2.7/site-packages:$PYTHONPATH
#[[ -s "$HOME/.pythonbrew/etc/bashrc" ]] && source "$HOME/.pythonbrew/etc/bashrc"
source ~/.rvm/scripts/rvm

alias rm='rmtrash'
alias vi='env LANG=ja_JP.UTF-8 /Applications/MacVim.app/Contents/MacOS/Vim "$@"'
alias vim='env LANG=ja_JP.UTF-8 /Applications/MacVim.app/Contents/MacOS/Vim "$@"'
alias mvim='env LANG=ja_JP.UTF-8 /Applications/MacVim.app/Contents/MacOS/Vim -g "$@"'

. ~/.node/nvm.sh

export PATH="$HOME/bin:$PATH"
alias mvi="mvim --remote-tab-silent"

if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

cd ~/

