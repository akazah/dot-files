;; Ricty {{{2 (http://save.sys.t.u-tokyo.ac.jp/~yusa/fonts/ricty.html)
(set-face-attribute 'default nil
                   :family "Ricty Discord"
                   :height 135)
(set-fontset-font
 nil 'japanese-jisx0208
 (font-spec :family "Ricty Discord"))
