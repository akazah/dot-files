;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; For folding
;;;
(let ((ruby-mode-hs-info
       '(ruby-mode
          "class\\|module\\|def\\|if\\|unless\\|case\\|while\\|until\\|for\\|begin\\|do"
          "end"
          "#"
          ruby-move-to-block
          nil)))
  (if (not (member ruby-mode-hs-info hs-special-modes-alist))
      (setq hs-special-modes-alist
            (cons ruby-mode-hs-info hs-special-modes-alist))))

;; C coding style
(add-hook 'c-mode-hook
          '(lambda ()
    (hs-minor-mode 1)))
;; Scheme coding style
(add-hook 'scheme-mode-hook
          '(lambda ()
    (hs-minor-mode 1)))
;; Elisp coding style
(add-hook 'emacs-lisp-mode-hook
          '(lambda ()
    (hs-minor-mode 1)))
;; Lisp coding style
(add-hook 'lisp-mode-hook
          '(lambda ()
    (hs-minor-mode 1)))
;; Python coding style
(add-hook 'python-mode-hook
          '(lambda ()
    (hs-minor-mode 1)))
;; Ruby coding style
(add-hook 'ruby-mode-hook
          '(lambda ()
    (hs-minor-mode 1)))
;; HTML coding style
(add-hook 'html-mode-hook
          '(lambda ()
    (hs-minor-mode 1)))
;; CSS coding style
(add-hook 'css-mode-hook
          '(lambda ()
    (hs-minor-mode 1)))
;; YAML coding style
(add-hook 'yaml-mode-hook
          '(lambda ()
    (hs-minor-mode 1)))

(require 'cn-outline)
(setq-default cn-outline-mode t)
(global-set-key (kbd "C-c C-c C-c") 'cn-outline-mode)

(define-key
  global-map
  (kbd "C-#") 'hs-toggle-hiding)
