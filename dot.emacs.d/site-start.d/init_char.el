;; 文字コード
;;(set-language-environment 'Japanese)
(set-language-environment  'utf-8)
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8-unix)

;; Ricty {{{2 (http://save.sys.t.u-tokyo.ac.jp/~yusa/fonts/ricty.html)
(set-face-attribute 'default nil
                   :family "Ricty Discord"
                   :height 135)
(set-fontset-font
 nil 'japanese-jisx0208
 (font-spec :family "Ricty Discord"))

 ;; ;; まず、install-elisp のコマンドを使える様にします。
 ;; (require 'install-elisp)
 ;; ;; 次に、Elisp ファイルをインストールする場所を指定します。
 ;; (setq install-elisp-repository-directory "~/.emacs.d/elisp/")