;(require 'elscreen)

;;
;; elscreen
;; ref http://www.morishima.net/~naoto/elscreen-ja/
;; ref http://d.hatena.ne.jp/rakudaininja/20090316/p1
;;______________________________________________________________________

(load "elscreen" "ElScreen" t)

;; タブを表示(非表示にする場合は nil を設定する)
(setq elscreen-display-tab t)

;; 自動でスクリーンを作成
(defmacro elscreen-create-automatically (ad-do-it)
  `(if (not (elscreen-one-screen-p))
       ,ad-do-it
     (elscreen-create)
     (elscreen-notify-screen-modification 'force-immediately)
     (elscreen-message "New screen is automatically created")))

(defadvice elscreen-next (around elscreen-create-automatically activate)
  (elscreen-create-automatically ad-do-it))

(defadvice elscreen-previous (around elscreen-create-automatically activate)
  (elscreen-create-automatically ad-do-it))

(defadvice elscreen-toggle (around elscreen-create-automatically activate)
  (elscreen-create-automatically ad-do-it))

;; タブ移動を簡単に
(define-key global-map (kbd "M-t") 'elscreen-next)

;; frame-titleにスクリーンの一覧を表示する
;; (defun elscreen-frame-title-update ()
;;   (when (elscreen-screen-modified-p 'elscreen-frame-title-update)
;;     (let* ((screen-list (sort (elscreen-get-screen-list) '<))
;;        (screen-to-name-alist (elscreen-get-screen-to-name-alist))
;;        (title (mapconcat
;;            (lambda (screen)
;;              (format "%d%s %s"
;;                  screen (elscreen-status-label screen)
;;                  (get-alist screen screen-to-name-alist)))
;;            screen-list " ")))
;;       (if (fboundp 'set-frame-name)
;;       (set-frame-name title)
;;     (setq frame-title-format title)))))

;; (eval-after-load "elscreen"
;;   '(add-hook 'elscreen-screen-update-hook 'elscreen-frame-title-update))

 	
