   (require 'windows)
   (win:startup-with-window)
   (define-key ctl-x-map "C" 'see-you-again)

;; ;(require 'elscreen)

;; ;;
;; ;; elscreen
;; ;; ref http://www.morishima.net/~naoto/elscreen-ja/
;; ;; ref http://d.hatena.ne.jp/rakudaininja/20090316/p1
;; ;;______________________________________________________________________

;; (load "elscreen" "ElScreen" t)

;; ;; タブを表示(非表示にする場合は nil を設定する)
;; (setq elscreen-display-tab t)

;; ;; 自動でスクリーンを作成
;; (defmacro elscreen-create-automatically (ad-do-it)
;;   `(if (not (elscreen-one-screen-p))
;;        ,ad-do-it
;;      (elscreen-create)
;;      (elscreen-notify-screen-modification 'force-immediately)
;;      (elscreen-message "New screen is automatically created")))

;; (defadvice elscreen-next (around elscreen-create-automatically activate)
;;   (elscreen-create-automatically ad-do-it))

;; (defadvice elscreen-previous (around elscreen-create-automatically activate)
;;   (elscreen-create-automatically ad-do-it))

;; (defadvice elscreen-toggle (around elscreen-create-automatically activate)
;;   (elscreen-create-automatically ad-do-it))

;; ;; タブ移動を簡単に
;; (define-key global-map (kbd "M-t") 'elscreen-next)

;; ;; frame-titleにスクリーンの一覧を表示する
;; ;; (defun elscreen-frame-title-update ()
;; ;;   (when (elscreen-screen-modified-p 'elscreen-frame-title-update)
;; ;;     (let* ((screen-list (sort (elscreen-get-screen-list) '<))
;; ;;        (screen-to-name-alist (elscreen-get-screen-to-name-alist))
;; ;;        (title (mapconcat
;; ;;            (lambda (screen)
;; ;;              (format "%d%s %s"
;; ;;                  screen (elscreen-status-label screen)
;; ;;                  (get-alist screen screen-to-name-alist)))
;; ;;            screen-list " ")))
;; ;;       (if (fboundp 'set-frame-name)
;; ;;       (set-frame-name title)
;; ;;     (setq frame-title-format title)))))

;; ;; (eval-after-load "elscreen"
;; ;;   '(add-hook 'elscreen-screen-update-hook 'elscreen-frame-title-update))



(defun my-window-size-save ()
  (let* ((rlist (frame-parameters (selected-frame)))
         (ilist initial-frame-alist)
         (nCHeight (frame-height))
         (nCWidth (frame-width))
         (tMargin (if (integerp (cdr (assoc 'top rlist)))
                      (cdr (assoc 'top rlist)) 0))
         (lMargin (if (integerp (cdr (assoc 'left rlist)))
                      (cdr (assoc 'left rlist)) 0))
         buf
         (file "~/.framesize.el"))
    (if (get-file-buffer (expand-file-name file))
        (setq buf (get-file-buffer (expand-file-name file)))
      (setq buf (find-file-noselect file)))
    (set-buffer buf)
    (erase-buffer)
    (insert (concat
             ;; 初期値をいじるよりも modify-frame-parameters
             ;; で変えるだけの方がいい?
             "(delete 'width initial-frame-alist)\n"
             "(delete 'height initial-frame-alist)\n"
             "(delete 'top initial-frame-alist)\n"
             "(delete 'left initial-frame-alist)\n"
             "(setq initial-frame-alist (append (list\n"
             "'(width . " (int-to-string nCWidth) ")\n"
             "'(height . " (int-to-string nCHeight) ")\n"
             "'(top . " (int-to-string tMargin) ")\n"
             "'(left . " (int-to-string lMargin) "))\n"
             "initial-frame-alist))\n"
             ;;"(setq default-frame-alist initial-frame-alist)"
             ))
    (save-buffer)
    ))

(defun my-window-size-load ()
  (let* ((file "~/.framesize.el"))
    (if (file-exists-p file)
        (load file))))

(my-window-size-load)

;; Call the function above at C-x C-c.
(defadvice save-buffers-kill-emacs
  (before save-frame-size activate)
  (my-window-size-save))
