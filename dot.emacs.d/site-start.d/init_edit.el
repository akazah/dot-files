;; Wthite Space mode
  (setq whitespace-space-regexp "\\(\u3000+\\)")
  (global-whitespace-mode t)
  (global-set-key (kbd "C-x w") 'global-whitespace-mode)

;; Soft tab
(setq-default tab-width 2 indent-tabs-mode nil)

(define-key global-map [?¥] [?\\])  ;; ¥の代わりにバックスラッシュを入力する

(setq comment-style 'multi-line)

(setq cssm-indent-function #'cssm-c-style-indenter)