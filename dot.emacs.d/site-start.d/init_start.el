;;; This was installed by package-install.el.
;;; This provides support for the package system and
;;; interfacing with ELPA, the package archive.
;;; Move this code earlier if you want to reference
;;; packages in your .emacs.
(when
    (load
     (expand-file-name "~/.emacs.d/elpa/package.el"))
  (package-initialize))


;;(require 'auto-complete)
;;(global-auto-complete-mode t)
;;(require 'snipplr.el)
(require 'less-css-mode)
(require 'eproject)
(require 'yaml-mode)
(require 'grep-edit)
;;(require 'rvm)
;;(rvm-use-default)

(setq howm-menu-lang 'ja)
(require 'howm-mode)

(setq backup-directory-alist
          `((".*" . ,temporary-file-directory)))
    (setq auto-save-file-name-transforms
          `((".*" ,temporary-file-directory t)))

(require 'backup-dir)
(setq bkup-backup-directory-info
       '((t               ".backups/"
                          full-path prepend-name search-upward)))

(add-hook 'yaml-mode-hook
		  '(lambda ()
			 (define-key yaml-mode-map "\C-m" 'newline-and-indent)))


			(require 'session)
			(add-hook 'after-init-hook 'session-initialize)
(setq session-undo-check -1)
 	


(require 'zencoding-mode)
(add-hook 'sgml-mode-hook 'zencoding-mode) ;; Auto-start on any markup modes

(define-key global-map [ns-drag-file] 'ns-find-file)


;;(set-frame-parameter (selected-frame) 'alpha '(85 50))

(defun my-html-format-region (begin end)
  "リージョンの HTML を整形する"
  (interactive "r")
  (unless (executable-find "~/bin/hamcutlet.rb")
    (error "hamcutlet.rb を利用できません"))
  (let ((text (buffer-substring-no-properties begin end)))
    (delete-region begin end)
    (call-process "~/bin/hamcutlet.rb" nil t 0 text)))

(defalias 'htmlf 'my-html-format-region)
;;(global-set-key (kbd "M-h") 'my-html-format-region) ;キーを割り当てる場合


(global-set-key "\C-m" 'newline-and-indent)

(require 'anything-startup)

(require 'auto-install)
(setq auto-install-directory "~/.emacs.d/auto-lisp/")
(auto-install-compatibility-setup)             ; 互換性確保

;;ATOK24.0の場合
;;「*scratch*」バッファでIMを有効化した状態で「(mac-get-current-in
(setq default-input-method "MacOSX")

